import path from 'path';
import {
  writeFileSync,
  readFileSync,
  opendirSync
} from 'fs'


const DIRNAME = `${path.resolve()}`
const FOLDER = `${DIRNAME}/../`

const getMeta = (suffix) => {
  let meta = []
  let file
  let svg
  const svgPath = `${FOLDER}/svg-${suffix}/svg`
  const svgFolder = opendirSync(`${svgPath}`)
  while ((file = svgFolder.readSync()) !== null) {
    svg = readFileSync(`${svgPath}/${file.name}`, 'utf-8');
    meta.push({
      name: `${file.name}`.slice(0,-4),
      viewbox: svg.match(/ viewBox="([^"]+)"/)[1],
      path: svg.match(/ d="([^"]+)"/)[1]
    })
  }
  return meta
}

const write = (file, data) => {
  writeFileSync(file, data)
}

export default {
  getMeta,
  write
}
